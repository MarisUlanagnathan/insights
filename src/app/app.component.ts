// import { Component, HostListener } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import {
  faColumns,
  faWindowClose,
  faTrash,
  faPlus,
  faMinus,
  fas,
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  constructor() {}

  title = "insight";
  heightOfWindow: any = "70";
  plusIcon = faPlus;
  trash = faTrash;
  minusIcon = faMinus;
  small = fas;
  result: any = {
    layout: [
      {
        id: 1,
        rowName: `R1`,
        rowHeight: "100",
        paddingTop: "0",
        paddingBottom: "0",

        paddingLeft: "0",
        paddingRight: "0",
        marginTop: "0",
        marginBottom:'0',
        marginLeft: "0",
        marginRight:'0',
        isRowHeightChanged: false,
        columns: [
          {
            id: 1,
            columnName: `C1`,
            columnWidth: 100,
            columnHeight: 100,
            isColumnHeightChanged: false,
            isColumnWidthChanged: false,
            selectedCellBorder: "",
            borderColor: "1px solid gray",
            paddingTop: "0",
            paddingBottom: "0",
    
            paddingLeft: "0",
            paddingRight: "0",
            marginTop: "0",
            marginBottom:'0',
            marginLeft: "0",
            marginRight:'0',
          },
        ],
      },
    ],
    borderColor: "0.5px solid gray",
    borderStyle: "rectangle",
    rowandColumnRounded: false,
    isOutlineNeeded: true,
    isHeaderNeeded: false,
    isFooterNeeded: false,
    headerHeight: 50,
    footerHeight: 25,
    margin:0,
    padding:0,
    layoutName:''
  };
  overAllCellMarigin = 0;
  overAllCellPadding = 0;
  layoutName:string = '';

  oldX = 2;
  columnId = 1;
  objectKeys = Object.keys;

  listOfRows = ["R1"];
  faColumn = faColumns;
  faClose = faWindowClose;
  width: number = 0;
  height: number = 0;
  ToMatchPreviousRowName = "";
  previousRowName = "";
  showSelectedCellSideBar: boolean = false;
  cellWidth = 100;
  rowHeight = 100;
  calculateCellWidth = 1;
  sideRowHeader = "";
  sideColumnHeader = "";
  isOutlineNeeded: boolean = true;
  isHeaderNeeded: boolean = false;

  isFooterNeeded: boolean = false;
  isStyleForBorder: string = "rectangle";
  showColor: boolean = false;
  hideProperties: boolean = true;
  borderColorChoosed: string = "1px solid teal";
  selectedColor: string = "teal";
  rowandColumnRounded: boolean = false;
  rowandColumnRoundedSize: string = "15px";
  rowandColumnRectangle: boolean = true;
  headerError: boolean = false;
  footerError: boolean = false;
  headerErrorValue:string = "";
  footerErrorValue:string = '';
  headerValue: string = "40";
  footerValue: string = "20";
  notSame: any = [];
  cellSize: string = "";
  selectedRow: any = {
    rowName: "",
    rowHeight: "",
    marginLeft: "",
    marginRight: "",
    marginTop: "",
    marginBottom: "",
    paddingTop: "",
    paddingBottom: "",
    paddingLeft: "",
    paddingRight: "",
  };
  selectedcellBoder = "";

  selectedCell: any = {
    colName: "",
    colWidth: "",
    colHeight: "",
    marginLeft: "",
    marginRight: "",
    marginTop: "",
    marginBottom: "",
    paddingTop: "",
    paddingBottom: "",
    paddingLeft: "",
    paddingRight: "",
  };

  ngOnInit() {
    if (this.result.layout.length !== 0) {
      this.cellClick({ rowName: "R1", colName: "C1" });
      this.selectedcellBoder = "2px solid black";
    }
  }

  ChangelayoutName = (event:any) => {
    this.layoutName = event.target.value
    this.result.layoutName= event.target.value
  }

  headerFooterValue = (typed: any, type: string) => {
    if (type === "header") {
      if(Number(typed.target.value) < 40 === true){
        this.headerErrorValue = 'Minimum height Should 40px'
        this.headerError = true;
        this.headerValue = "";
      }
      else if(Number(typed.target.value) > 100 === true) {
        this.headerError = true;
        this.headerValue = "";
        this.headerErrorValue = 'Maximum height Should be 100px'

      } 
      else {
        this.headerError = false;

        this.headerValue = typed.target.value;
        this.result.headerHeight = typed.target.value;
        this.headerErrorValue = ''
      }
    } else if (type === "footer") {
      if(Number(typed.target.value) < 20 === true){
        this.footerError = true;
        this.footerValue = "";
        this.footerErrorValue = 'Minimum height Should be 20px'
      }

      else if (Number(typed.target.value) > 50) {
        this.footerError = true;
        this.footerValue = "";
        this.footerErrorValue = 'Maximum height Should be 50px'

      } else {
        this.footerError = false;

        this.footerValue = typed.target.value;
        this.result.footerHeight = typed.target.value;
        this.footerErrorValue = ''
      }
    }else if(type === 'cellMarigin'){
      this.overAllCellMarigin = typed.target.value
      this.result.margin =  typed.target.value
      this.result.layout.forEach((data:any) => {
        data.marginLeft = typed.target.value;
        data.marginRight =typed.target.value;
        data.marginTop = typed.target.value;
        data.marginBottom = typed.target.value;
        
        data.columns.forEach((cell:any) => {
          cell.marginLeft = typed.target.value;
          cell.marginRight = typed.target.value;
          cell.marginTop = typed.target.value;
          cell.marginBottom = typed.target.value;
        })
        this.selectedRow.marginLeft = typed.target.value;
        this.selectedRow.marginRight = typed.target.value;
        this.selectedRow.marginTop = typed.target.value;
        this.selectedRow.marginBottom = typed.target.value;
        this.selectedCell.marginLeft = typed.target.value;
        this.selectedCell.marginRight = typed.target.value;
        this.selectedCell.marginTop = typed.target.value;
        this.selectedCell.marginBottom = typed.target.value;

      })
    }else if(type === 'cellPadding'){
      this.overAllCellPadding = typed.target.value
      this.result.padding =  typed.target.value

      this.result.layout.forEach((data:any) => {
        data.paddingLeft = typed.target.value;
        data.paddingRight =typed.target.value;
        data.paddingTop = typed.target.value;
        data.paddingBottom = typed.target.value;
        data.columns.forEach((cell:any) => {
          cell.paddingLeft = typed.target.value;
          cell.paddingRight = typed.target.value;
          cell.paddingTop = typed.target.value;
          cell.paddingBottom = typed.target.value;
        })
      })
      this.selectedRow.paddingLeft = typed.target.value;
        this.selectedRow.paddingRight = typed.target.value;
        this.selectedRow.paddingTop = typed.target.value;
        this.selectedRow.paddingBottom = typed.target.value;
        this.selectedCell.paddingLeft = typed.target.value;
        this.selectedCell.paddingRight = typed.target.value;
        this.selectedCell.paddingTop = typed.target.value;
        this.selectedCell.paddingBottom = typed.target.value;
    }
    
    else {
      return;
    }
  };

  deleteSpecificRowsAndCells = (rowName:any,cellName:any) => {
    if(this.result.layout.length !== 0){
    let lastRow = this.result.layout[this.result.layout.length - 1]  
    this.result.layout.forEach((data:any,indexRow:any) => {
      if(cellName === "" && data.rowName === rowName){
        let splitRowName = rowName.split('')[1];
        let currentRowCell = `R${splitRowName}`
        let nextRow = lastRow.rowName === currentRowCell ? `R${Number(splitRowName) - 1}` : `R${Number(splitRowName) + 1}` ;
        this.result.layout.splice(indexRow,1)
        this.result.layout.map((eachRow: any) => {
          eachRow.rowHeight =
            Number(this.rowHeight) / Number(this.result.layout.length);
          this.selectedRow.rowHeight =
            Number(this.rowHeight) / Number(this.result.layout.length);
        });
        if(currentRowCell === rowName && this.result.layout.length !== 0){
          this.cellClick({ rowName: nextRow, colName: 'C1'});

        }else{
          this.hideProperties = false
          this.showSelectedCellSideBar = false
        }

      }else if(cellName !== '' && data.rowName === rowName){
        let splitCellName = cellName.split('')[1];
        let currentRowCell = `C${splitCellName}`
        let lastFrontRow = this.result.layout.length >1 ? this.result.layout[this.result.layout.length - 2] : ''
        let lastCell = data.columns[data.columns.length - 1]
        let lastFrontCell = data.columns.length !== 0 ? data.columns[data.columns.length - 2] : 'C0'

        let nextCell = lastCell.columnName === currentRowCell ? lastFrontCell?.columnName === undefined ? lastFrontCell: lastFrontCell.columnName : `C${Number(splitCellName) + 1}` ;
        data.columns.forEach((cellData:any,index:any) => {
         
          if (cellData.columnName === cellName){
            
            data['columns'].splice(index,1)

          }
        })
        
        this.result.layout.map((element: any) => {
          if (element.rowName === rowName && element.columns.length !== 0) {
            element.columns.map((eachCell: any) => {
              if(lastCell.columnName === cellName){
                element.rowHeight =
                Number(this.rowHeight) / Number(this.result.layout.length);
              this.selectedRow.rowHeight =
                Number(this.rowHeight) / Number(this.result.layout.length);
                eachCell.columnWidth =
                Number(this.cellWidth) / Number(element.columns.length);
              this.selectedCell.colWidth =
                Number(this.cellWidth) / Number(element.columns.length);
              }else {
                eachCell.columnWidth =
                Number(this.cellWidth) / Number(element.columns.length);
              this.selectedCell.colWidth =
                Number(this.cellWidth) / Number(element.columns.length);
              }
            
            });
          }
        });
        if(nextCell === undefined){
          this.result.layout.splice(indexRow,1)

          this.result.layout.forEach((datas:any) => {
            
            datas.rowHeight = Number(this.rowHeight)/this.result.layout.length
          })
          if(lastFrontRow ?.rowName !== undefined){
            this.cellClick({ rowName: lastFrontRow.rowName, colName: 'C1'});

          }else{
            this.hideProperties = false
            this.showSelectedCellSideBar = false
          }

        }
        else if(currentRowCell === cellName && this.result.layout.length !== 0){
          this.cellClick({ rowName: rowName, colName: nextCell});

        }else{
          this.hideProperties = false
          this.showSelectedCellSideBar = false
        }
        return this.result;
      }
    })
    return this.result
  }
  }

  DeleteRow = () => {
    if (this.result.layout.length > 1) {
      this.result.layout.pop();
      this.result.layout.map((eachRow: any) => {
        eachRow.rowHeight =
          Number(this.rowHeight) / Number(this.result.layout.length);
        this.selectedRow.rowHeight =
          Number(this.rowHeight) / Number(this.result.layout.length);
      });
      this.oldX = this.oldX - 1;
      return this.result;
    } 
  };

  styleForRowAndColumn = (typeOfStyle: string) => {
    if (typeOfStyle === "rounded") {
      this.rowandColumnRectangle = false;
      this.result.rowandColumnRounded = true;

      this.rowandColumnRounded = true;
      this.result.borderStyle = "rounded";
    } else if (typeOfStyle === "rectangle") {
      this.rowandColumnRounded = false;
      this.rowandColumnRectangle = true;
      this.result.borderStyle = "recatangle";
      this.result.rowandColumnRounded = false;
    }
  };

  handleBorderColorChange = (event: any) => {
    this.borderColorChoosed = `1px solid ${event.color.hex}`;
    this.selectedColor = event.color.hex;
    this.result.borderColor = `1px solid ${event.color.hex}`;
    this.hideProperties = !this.hideProperties;
    this.showColor = !this.showColor;
    this.result.layout.map((eachData: any) => {
      eachData.columns.map((cell: any) => {
        cell["borderColor"] = `1px solid ${event.color.hex}`;
      });
    });
  };

  showColorPalette = () => {
    this.hideProperties = !this.hideProperties;
    this.showColor = !this.showColor;
  };

  cellClick = (event: any) => {
    this.selectedcellBoder = "";

    let rowName = event.rowName;
    let colName = event.colName;

    this.showSelectedCellSideBar = true;
    (this.sideColumnHeader = colName), (this.sideRowHeader = rowName);
    this.result.layout.forEach((data: any) => {
      if (data.rowName === rowName && data.columns.length !== 0) {
        this.selectedRow = {
          rowName: data.rowName,
          rowHeight: data.rowHeight,
          marginLeft: data.marginLeft !== undefined ? data.marginLeft : "",
          marginRight: data.marginRight !== undefined ? data.marginRight : "",
          marginTop: data.marginTop !== undefined ? data.marginTop : "",
          marginBottom:
            data.marginBottom !== undefined ? data.marginBottom : "",
          paddingTop: data.paddingTop !== undefined ? data.paddingTop : "",
          paddingBottom:
            data.paddingBottom !== undefined ? data.paddingBottom : "",
          paddingLeft: data.paddingLeft !== undefined ? data.paddingLeft : "",
          paddingRight:
            data.paddingRight !== undefined ? data.paddingRight : "",
        };
        data.columns.forEach((cell: any) => {
          if (cell.columnName === colName) {
            this.selectedCell = {
              colName: cell.columnName,
              colWidth: cell.columnWidth,
              colHeight: cell.columnHeight,
              marginLeft: cell.marginLeft !== undefined ? cell.marginLeft : "",
              marginRight:
                cell.marginRight !== undefined ? cell.marginRight : "",
              marginTop: cell.marginTop !== undefined ? cell.marginTop : "",
              marginBottom:
                cell.marginBottom !== undefined ? cell.marginBottom : "",
              paddingTop: cell.paddingTop !== undefined ? cell.paddingTop : "",
              paddingBottom:
                cell.paddingBottom !== undefined ? cell.paddingBottom : "",
              paddingLeft:
                cell.paddingLeft !== undefined ? cell.paddingLeft : "",
              paddingRight:
                cell.paddingRight !== undefined ? cell.paddingRight : "",
            };
            this.cellSize = cell.columnWidth;
            cell["selectedCellBorder"] = "2px solid black";

            this.removeSelectedBorder(data, cell);
          }
        });
      }
    });
    return this.selectedRow, this.selectedCell;
  };
  removeSelectedBorder = (rowData: any, currentCell: any) => {
    rowData["columns"].map((eachCell: any) => {
      if (eachCell.id !== currentCell.id) {
        eachCell["selectedCellBorder"] = "";
      }
    });
    this.result.layout.map((rows: any) => {
      if (rows.rowName !== rowData.rowName) {
        rows.columns.map((data: any) => {
          data["selectedCellBorder"] = "";
        });
      }
    });
    return this.result;
  };

  onChangeOutlineEvent = (value: any, type: string) => {
    if (type === "Outline") {
      this.isOutlineNeeded = value;
      this.result.isOutlineNeeded = value;
    } else if (type === "Header") {
      this.isHeaderNeeded = value;
      this.result.isHeaderNeeded = value;
    } else if (type === "Footer") {
      this.isFooterNeeded = value;
      this.result.isFooterNeeded = value;
    }
  };

  checkForRowValues = (rowData: any, typedValue: any, type: any) => {
    if (type === "less") {
      this.result.layout.forEach((data: any) => {
        if (data.rowName !== rowData.rowName) {
          let value = data.rowHeight - typedValue;

          data["rowHeight"] = value;
        }
      });

      return this.result;
    } else if (type === "great") {
      var added = 0;

      this.result.layout.map((daat: any, index: number) => {
        if (daat.isRowHeightChanged === true) {
          added = added + Number(this.result.layout[index].rowHeight);
        }
      });
      let newPic = this.result.layout.filter((daat: any) => {
        return daat.isRowHeightChanged !== true;
      });
      if (added !== 100) {
        this.result.layout.forEach((data: any) => {
          if (data.id !== rowData.id && data.isRowHeightChanged !== true) {
            let value =
              (Number(this.rowHeight) - Number(added)) / newPic.length;
            data["rowHeight"] = `${value}`;
          }
        });
      } else {
        this.result.layout.pop();
        this.oldX = this.oldX - 1;
      }
      return this.result;
    } else {
      return;
    }
  };

  checkForColumnValues = (
    rowData: any,
    columnData: any,
    typedValue: any,
    type: any
  ) => {
    if (type === "width") {
      var added = 0;
      rowData["columns"].map((daat: any, index: number) => {
        if (daat.isColumnWidthChanged === true) {
          added = added + Number(rowData["columns"][index].columnWidth);
        }
      });
      let calculateColumn = rowData["columns"].filter((daat: any) => {
        return daat.isColumnWidthChanged !== true;
      });
      let cellWidthLess = rowData["columns"].filter((daat: any) => {
        return Number(daat.columnHeight) <= 50;
      });

      if (added !== 100) {
        if (cellWidthLess.length !== 0 && cellWidthLess.length > 1) {
          return this.result
        } else {
          rowData["columns"].map((data: any) => {
            if (
              data.isColumnWidthChanged === false &&
              data.id !== columnData.id
            ) {
              data["columnWidth"] =
                (Number(this.cellWidth) - Number(added)) /
                calculateColumn.length;
            } else if (
              data.isColumnWidthChanged === true &&
              data.id !== columnData.id
            ) {
              return this.result
            }
          });
        }
      } else {
        rowData["columns"].pop();
      }
      return rowData;
    } 
  };

  checkfrom = (value: any) => {
    let check = ["95", "96", "97", "98", "99", "100"];
    let checkings = check.includes(value);

    return checkings === true ? alert("value exceeds") : value;
  };

  inputMarginChange = (
    event: any,
    side: string,
    RowHead: string,
    ColHead: string,
    type: string
  ) => {
    this.result.layout.forEach((element: any) => {
      if (type === "row" && element.rowName === RowHead) {
        
          if (side === "L") {
            if (!element["marginLeft"]) {
              element["marginLeft"] = event.target.value;
              this.selectedRow.marginLeft = event.target.value;
            } else {
              element["marginLeft"] = event.target.value;
              this.selectedRow.marginLeft = event.target.value;
            }
          } else if (side === "R") {
            if (!element["marginRight"]) {
              element["marginRight"] = event.target.value;
              this.selectedRow.marginRight = event.target.value;
            } else {
              element["marginRight"] = event.target.value;
              this.selectedRow.marginRight = event.target.value;
            }
          } else if (side === "T") {
            if (!element["marginTop"]) {
              element["marginTop"] = event.target.value;
              this.selectedRow.marginTop = event.target.value;
            } else {
              element["marginTop"] = event.target.value;
              this.selectedRow.marginTop = event.target.value;
            }
          } else if (side === "B") {
            if (!element["marginBottom"]) {
              element["marginBottom"] = event.target.value;
              this.selectedRow.marginBottom = event.target.value;
            } else {
              element["marginBottom"] = event.target.value;
              this.selectedRow.marginBottom = event.target.value;
            }
          } else if (side === "size") {
            if (!element["rowHeight"]) {
              element["rowHeight"] = event.target.value;
            } else {
              if (this.result.layout.length > 1) {
                let checkedResult = this.checkfrom(event.target.value);
                if (checkedResult !== undefined) {
                  if (checkedResult > element.rowHeight) {
                    element["rowHeight"] = checkedResult;
                    element["isRowHeightChanged"] = true;
                    this.checkForRowValues(element, checkedResult, "great");
                  } else if (checkedResult < element.rowHeight) {
                    element["rowHeight"] = event.target.value;

                    this.checkForRowValues(element, checkedResult, "less");
                  }
                }
              } else {
                if (Number(event.target.value) < 10) {
                  element["rowHeight"] = "30";

                  this.checkForRowValues(element, "30", "less");
                } else {
                  if (event.target.value > element.rowHeight) {
                    element["rowHeight"] = event.target.value;

                    this.checkForRowValues(
                      element,
                      event.target.value,
                      "great"
                    );
                  } else if (event.target.value < element.rowHeight) {
                    element["rowHeight"] = event.target.value;
                    this.selectedRow.rowHeight = event.target.value;
                    this.checkForRowValues(element, event.target.value, "less");
                  }
                }
              }
            }
          }
        
      } else if (type === "col" && element.rowName === RowHead) {
          element.columns.map((data: any) => {
            if (data.columnName === ColHead) {
              if (side === "L" && data['marginLeft']) {
               
                  data["marginLeft"] = event.target.value;
                  this.selectedCell.marginLeft = event.target.value;
                
              } else if (side === "R" && data["marginRight"]) {
               
                  data["marginRight"] = event.target.value;
                  this.selectedCell.marginRight = event.target.value;
                
              } else if (side === "T" && data["marginTop"]) {
               
                  data["marginTop"] = event.target.value;
                  this.selectedCell.marginTop = event.target.value;
                
              } else if (side === "B" && data["marginBottom"]) {
               
                  data["marginBottom"] = event.target.value;
                  this.selectedCell.marginBottom = event.target.value;
                
              } else if (side === "size" && data["columnWidth"]) {
               
                  data["columnWidth"] = event.target.value;
                  data["isColumnWidthChanged"] = true;
                  this.selectedCell.colWidth = event.target.value;
                  this.checkForColumnValues(
                    element,
                    data,
                    event.target.value,
                    "width"
                  );
                
              } else if (side === "height" && data["columnHeight"]) {
                
                
                  data["columnHeight"] = event.target.value;
                  data["isColumnHeightChanged"] = true;

                  this.selectedCell.colHeight = event.target.value;
                  this.checkForColumnValues(
                    element,
                    data,
                    event.target.value,
                    "height"
                  );
                
              }
            }
          });
        
      }else{
        return this.result
      }
    });
    return this.result;
  };
  inputPaddingChange = (
    event: any,
    side: string,
    RowHead: string,
    ColHead: string,
    type: string
  ) => {
    this.result.layout.forEach((element: any) => {
      if (type === "row" && element.rowName === RowHead) {
          if (side === "L" && element["paddingLeft"]) {
           
              element["paddingLeft"] = event.target.value;
              this.selectedRow.paddingLeft = event.target.value;
            
          } else if (side === "R" && element["paddingRight"]) {
           
              element["paddingRight"] = event.target.value;
              this.selectedRow.paddingRight = event.target.value;
            
          } else if (side === "T" && element["paddingTop"]) {
           
              element["paddingTop"] = event.target.value;
              this.selectedRow.paddingTop = event.target.value;
            
          } else if (side === "B" && element["paddingBottom"] ) {
          
              element["paddingBottom"] = event.target.value;
              this.selectedRow.paddingBottom = event.target.value;
            
          }
        
      } else if (type === "col" && element.rowName === RowHead) {
          element.columns.map((data: any) => {
            if (data.columnName === ColHead) {
              if (side === "L") {
                if (!data["paddingLeft"]) {
                  data["paddingLeft"] = event.target.value;
                  this.selectedCell.paddingLeft = event.target.value;
                } else {
                  data["paddingLeft"] = event.target.value;
                  this.selectedCell.paddingLeft = event.target.value;
                }
              } else if (side === "R") {
                if (!data["paddingRight"]) {
                  data["paddingRight"] = event.target.value;
                  this.selectedCell.paddingRight = event.target.value;
                } else {
                  data["paddingRight"] = event.target.value;
                  this.selectedCell.paddingRight = event.target.value;
                }
              } else if (side === "T") {
                if (!data["paddingTop"]) {
                  data["paddingTop"] = event.target.value;
                  this.selectedCell.paddingTop = event.target.value;
                } else {
                  data["paddingTop"] = event.target.value;
                  this.selectedCell.paddingTop = event.target.value;
                }
              } else if (side === "B") {
                if (!data["paddingBottom"]) {
                  data["paddingBottom"] = event.target.value;
                  this.selectedCell.paddingBottom = event.target.value;
                } else {
                  data["paddingBottom"] = event.target.value;
                  this.selectedCell.paddingBottom = event.target.value;
                }
              }
            }
          });
        
      }
    });
    return this.result;
  };

  saveResult = () => {
    if(this.layoutName !== '' && this.result.layoutName !== ''){
      console.log(JSON.stringify(this.result), "result");

    }else{
      alert('Layout Name is mandatory')
    }
  };

  addRow = () => {
    if (this.result.layout.length < 4) {
      let calculateHeight = this.rowHeight / (this.result.layout.length + 1);
      this.result.layout.forEach((data: any) => {
        data.rowHeight = calculateHeight;
      });
      this.result.layout.push({
        id: this.oldX,
        rowName: `R${this.oldX}`,
        rowHeight: calculateHeight,
        paddingTop: this.overAllCellPadding,
        paddingBottom: this.overAllCellPadding,

        paddingLeft: this.overAllCellPadding,
        paddingRight: this.overAllCellPadding,
        marginTop:this.overAllCellMarigin,
        marginBottom:this.overAllCellMarigin,
        marginLeft: this.overAllCellMarigin,
        marginRight:this.overAllCellMarigin,
        isRowHeightChanged: false,
        columns: [
          {
            id: this.columnId,
            columnName: `C${this.columnId}`,
            columnWidth: this.cellWidth,
            columnHeight: 100,
            isColumnWidthChanged: false,
            selectedCellBorder: "",
            borderColor: "1px solid gray",
            paddingTop: "0",
            paddingBottom: "0",
    
            paddingLeft: "0",
            paddingRight: "0",
            marginTop: "0",
            marginBottom:'0',
            marginLeft: "0",
            marginRight:'0',
          },
        ],
      });
      this.cellClick({ rowName: `R${this.oldX}`, colName: `C${this.columnId}` });

      this.oldX = this.oldX + 1;
      this.hideProperties = true
    } else {
      alert("Only Four layout allowed");
    }
   

  };

  removeColumnToParticularRow = (id: any) => {
    if (id) {
      this.result.layout.map((element: any) => {
        if (element.rowName === id && element.columns.length > 1) {
          let lastCell = element.columns[element.columns.length - 1];
          element.columns.pop();
          element.columns.map((eachCell: any) => {
            eachCell.columnWidth =
              Number(this.cellWidth) / Number(element.columns.length);
            this.selectedCell.colWidth =
              Number(this.cellWidth) / Number(element.columns.length);
          });
        }
      });
      return this.result;
    } else {
      return;
    }
  };

  addColumnToParticularRow = (id: any) => {
    this.selectedcellBoder = "";

    if (id) {
      this.result.layout.map((element: any
        ) => {
        if (element.rowName === id) {
          this.ToMatchPreviousRowName = element.rowName;
          
          if (element?.columns.length < 5) {
            element.columns.map((data: any) => {
              data["columnWidth"] =
               Math.round(Number(this.cellWidth) / (element.columns.length + 1));
            });
            let lastCell = element.columns[element.columns.length - 1];
            element.columns.push({
              id: lastCell.id + 1,
              columnName: `C${lastCell.id + 1}`,
              columnWidth:
                Number(this.cellWidth) / (element.columns.length + 1),
              columnHeight: 100,
              isColumnHeightChanged: false,
              isColumnWidthChanged: false,
              selectedCellBorder: "",
              borderColor: "1px solid gray",
              paddingTop: this.overAllCellPadding,
              paddingBottom: this.overAllCellPadding,
      
              paddingLeft: this.overAllCellPadding,
              paddingRight: this.overAllCellPadding,
              marginTop: this.overAllCellMarigin,
              marginBottom:this.overAllCellMarigin,
              marginLeft: this.overAllCellMarigin,
              marginRight:this.overAllCellMarigin,
            });
            this.cellClick({ rowName: id, colName: `C${lastCell.id + 1}` });

            this.selectedCell.colWidth =
              this.cellWidth / element.columns.length;
          } else {
            alert("Only Five cells allowed");
          }
        }
      });
    }
  };
}

// git remote set-url origin https://gitlab.ust-global.work/insightsstudio/layout-editor