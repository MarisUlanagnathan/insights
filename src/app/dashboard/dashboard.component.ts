import { Component } from "@angular/core";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent {
  constructor() {}
  heightOfWindow: any = "100";
  result: any = {
   "layout":[
      {
         "id":1,
         "rowName":"R1",
         "rowHeight":"30",
         "paddingTop":"5",
         "paddingBottom":"5",
         "paddingLeft":"5",
         "paddingRight":"5",
         "marginTop":"5",
         "columns":[
            {
               "id":1,
               "columnName":"C1",
               "columnWidth":50,
               "columnHeight":""
            },
            {
               "id":2,
               "columnName":"C2",
               "columnWidth":"70",
               "columnHeight":20
            }
         ]
      },
      {
         "id":2,
         "rowName":"R2",
         "rowHeight":"70",
         "paddingTop":"5",
         "paddingBottom":"5",
         "paddingLeft":"5",
         "paddingRight":"5",
         "marginTop":"5",
         "columns":[
            {
               "id":1,
               "columnName":"C1",
               "columnWidth":100
            }
         ]
      }
   ],
   "borderColor":"0.5px solid gray",
   "borderStyle":"rectangle",
   "rowandColumnRounded":false,
   "isOutlineNeeded":true,
   "isHeaderNeeded":false,
   "isFooterNeeded":false,
   "headerHeight":50,
   "footerHeight":25
}
   
  

}
