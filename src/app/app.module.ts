import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KtdGridModule } from '@katoid/angular-grid-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';

import { ColorSketchModule } from 'ngx-color/sketch';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ColorCircleModule } from 'ngx-color/circle';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RowsAndCellsComponent } from './rows-and-cells/rows-and-cells.component'; // <color-circle></color-circle>
import { faPlus,faMinus,faColumns } from '@fortawesome/free-solid-svg-icons';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RowsAndCellsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    KtdGridModule,
    FontAwesomeModule,
    ColorCircleModule,
    ColorSketchModule, // added to imports
    UiSwitchModule.forRoot({
      size: 'small',
      color: 'teal',
      switchColor: '#teal',
      defaultBgColor: 'gray',
      defaultBoColor : 'gray',
      // checkedLabel: 'on',
      // uncheckedLabel: 'off'
    })


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
