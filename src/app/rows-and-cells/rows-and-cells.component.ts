import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-rows-and-cells",
  templateUrl: "./rows-and-cells.component.html",
  styleUrls: ["./rows-and-cells.component.css"],
})
export class RowsAndCellsComponent {
  @Input() result: any;
  @Input() heightOfWindow: any;
  @Input() selectedBorder:any;
  @Output() cellEvent = new EventEmitter<any>();

  getRow(colName: string, rowName: string) {
    let data = {
      rowName: rowName,
      colName: colName,
    };
    this.cellEvent.emit(data);
  }
}
